package homework8;


public abstract class GeometricFigure implements Reloc, Scale{
    private int x;
    private int y;

    public abstract int getArea();
    public abstract int getPerimeter();

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void relocatable(int x, int y){
        this.x = x;
        this.y = y;
}
}
