package homework8;

public class Ellipse extends GeometricFigure {
    private int BigHalfShaft;
    private int SmallHalfShaft;
    public Ellipse(int BigHalfShaft, int SmallHalfShaft){
        this.BigHalfShaft = BigHalfShaft;
        this.SmallHalfShaft = SmallHalfShaft;
    }
    public int getArea(){
        return (int) (Math.PI * BigHalfShaft * SmallHalfShaft);
    }
    public int getPerimeter(){
        return 4 * (getArea() + (BigHalfShaft - SmallHalfShaft) * (BigHalfShaft - SmallHalfShaft)) / (BigHalfShaft + SmallHalfShaft);
    }
    @Override
    public void scalable(int m) {
        this.BigHalfShaft = BigHalfShaft * m;
        this.SmallHalfShaft = SmallHalfShaft * m;
}
}

