package homework8;


public class Main {
    public static void main(String[] args) {
        GeometricFigure square = new Square(5);
        System.out.println("Площадь квадрата: " + square.getArea());
        System.out.println("Периметр квадрата: " + square.getPerimeter());

        GeometricFigure rectangle = new Rectangle(6,7);
        System.out.println("Площадь прямоугольника: " + rectangle.getArea());
        System.out.println("Периметр прямоугольника: " + rectangle.getPerimeter());

        GeometricFigure circle = new Circle(3);
        System.out.println("Площадь круга: " + circle.getArea());
        System.out.println("Периметр круга: " + circle.getPerimeter());

        GeometricFigure ellipse = new Ellipse(9,4);
        System.out.println("Площадь эллипса: " + ellipse.getArea());
        System.out.println("Периметр эллипса: " + ellipse.getPerimeter());

        System.out.println("Центр эллепса: " + ellipse.getX() + " " + ellipse.getY());
        ellipse.relocatable(4, 8);
        System.out.println("Центр эллепса: " + ellipse.getX() + " " + ellipse.getY());

        square.scalable(3);
        System.out.println("Площадь квадрата: " + square.getArea());
        System.out.println("Периметр квадрата: " + square.getPerimeter());

    }
}
