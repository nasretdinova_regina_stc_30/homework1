package homework8;

public class Rectangle extends GeometricFigure{
    private int width;
    private int length;
    public Rectangle(int length, int width){
        this.width = width;
        this.length = length;
    }
    public int getArea(){
        return width * length;
    }
    public int getPerimeter(){
        return 2 * (width + length);
    }

    @Override
    public void scalable(int m) {
        this.width = width * m;
        this.length = length * m;
    }
}
