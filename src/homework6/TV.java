package homework6;

public class TV {

    private Channel[] channels;

    public TV(Channel[] inputChannel){
        channels = inputChannel;
    }
    public void showChannel(int channelNumber){
        Channel selectedChannel = channels[channelNumber];
        selectedChannel.showRandomProgram();
    }
}
