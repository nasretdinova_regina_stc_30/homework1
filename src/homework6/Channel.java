package homework6;

public class Channel {

    private Program[] programs;

    public Channel(Program[] inputProgram) {
        programs = inputProgram;
    }
    public void showRandomProgram() {
        int ProgramCount = programs.length;
        int selectedProgramNumber = (int) (Math.random() * ProgramCount);
        Program selectedProgram = programs[selectedProgramNumber];
        String programName = selectedProgram.getName();
        System.out.println("Имя программы " + programName);
    }
}
