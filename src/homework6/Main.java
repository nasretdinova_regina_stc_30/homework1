package homework6;

public class Main {

    public static void main(String[] args) {

        Program[] programsForChannel0 = new Program[]{
                new Program("gaming"),
                new Program("working"),
                new Program("education")
        };

        Program[] programsForChannel1 = new Program[]{
                new Program("r"),
                new Program("u"),
                new Program("ll")
        };

        Program[] programsForChannel2 = new Program[]{
                new Program("o"),
                new Program("t"),
                new Program("i")
        };

        Channel[] channelsForTV = new Channel[]{
                new Channel(programsForChannel0),
                new Channel(programsForChannel1),
                new Channel(programsForChannel2)
        };

        TV myTV = new TV(channelsForTV);

        myTV.showChannel(1);
        }
}
