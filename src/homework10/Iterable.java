package homework10;

public interface Iterable {
    Iterator iterator();
}
