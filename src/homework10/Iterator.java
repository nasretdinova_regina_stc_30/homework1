package homework10;

public interface Iterator {
    int next();
    boolean hasNext();

}
