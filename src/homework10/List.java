package homework10;

public interface List extends Collection {
    int get(int index);
    int indexOf(int element);
    void removeByIndex(int index);
    int insert(int element, int index);
    void reverse();

}
