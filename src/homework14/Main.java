package homework14;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        System.out.println("Все автомобили в списке:");
        getCar().forEach(System.out::println);
        System.out.println("Задание номер 1: " +
                "Вывести номера всех автомобилей, имеющих черный цвет или нулевой пробег");
        getCar().stream()
                .filter(filter -> filter.getColour().equals("BLACK") || filter.getMileAge() == 0)
                .map(Car::getNumber).forEach(entity -> System.out.print(entity + " "));
        System.out.println();
        System.out.println("Задание номер 2: " +
                "Вывести количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс");
        int count = getCar().stream().filter(filter -> filter.getCost() >= 700000 && filter.getCost() <= 800000)
                .map(Car::getModel).collect(Collectors.toSet()).size();
        System.out.println(count);
        System.out.println("Задание номер 3: " +
                "Вывести цвет автомобиля с минимальной стоимостью");
        getCar().stream().min((car1, car2) -> car1.getCost() - car2.getCost()).ifPresent(System.out::println);

        System.out.println("Задание номер 4: " +
                "Рассчитать среднюю стоимость Camry");
        System.out.println(getCar().stream().filter(filter -> filter.getModel().equals("CAMRY"))
                .map(Car::getCost).mapToInt(Integer::intValue).average().getAsDouble());

    }

    public static List<Car> getCar() {
        try {
            List<Car> result = Files.lines(Paths.get("cars.txt")).map(line -> line.split("\\W+"))
                    .map(array -> new Car(Integer.parseInt(array[1]), array[2],
                            array[3], Integer.parseInt(array[4]),
                            Integer.parseInt(array[5]))).collect(Collectors.toList());
            return result;
        } catch (IOException e) {
            throw new IllegalArgumentException("Файл не найден!");
        }
    }
}

