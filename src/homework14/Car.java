package homework14;

import java.util.Objects;

public class Car implements Comparable<Car> {
    private int number;
    private String model;
    private String colour;
    private int mileAge;
    private int cost;

    public Car(int number, String model, String colour, int mileAge, int cost) {
        this.number = number;
        this.model = model;
        this.colour = colour;
        this.mileAge = mileAge;
        this.cost = cost;
    }

    public Car(String s, String model, String colour, long parseLong, long parseLong1) {
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public int getMileAge() {
        return mileAge;
    }

    public void setMileAge(int mileAge) {
        this.mileAge = mileAge;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "Car{" +
                "number=" + number +
                ", model='" + model + '\'' +
                ", colour='" + colour + '\'' +
                ", mileAge=" + mileAge +
                ", cost=" + cost +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return number == car.number &&
                mileAge == car.mileAge &&
                cost == car.cost &&
                Objects.equals(model, car.model) &&
                Objects.equals(colour, car.colour);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, model, colour, mileAge, cost);
    }



    @Override
    public int compareTo(Car car) {
        return this.model.hashCode() - car.getModel().hashCode();
    }
}
