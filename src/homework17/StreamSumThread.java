package homework17;

import java.util.concurrent.atomic.AtomicInteger;

public class StreamSumThread extends Thread {

    AtomicInteger sum;

    public StreamSumThread(AtomicInteger sum) {
        this.sum = sum;
    }

    @Override
    public void run() {
        int[] array;
        array = new int[100];
        for (int i = 0; i < 100; i++) {
            array[i] = 1;
        }
        for( int num : array) {
            sum.getAndAdd(num);
        }
        System.out.println(sum);
    }
}

