package homework17;

import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    public static void main(String[] args) throws InterruptedException {

        AtomicInteger sum = new AtomicInteger();

        StreamSumThread firstStreamSumThread = new StreamSumThread(sum);
        StreamSumThread secondStreamSumThread = new StreamSumThread(sum);
        StreamSumThread thirdStreamSumThread = new StreamSumThread(sum);

        firstStreamSumThread.start();
        secondStreamSumThread.start();
        thirdStreamSumThread.start();

        firstStreamSumThread.join();
        secondStreamSumThread.join();
        thirdStreamSumThread.join();
    }
}


