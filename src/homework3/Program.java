package homework3;
import java.util.Arrays;
import java.util.Scanner;

public class Program {

        public static void main(String[] args) {

            Scanner scanner = new Scanner(System.in);
            System.out.println("Введите размер массива: ");
            int n = scanner.nextInt();
            int[] array = new int[n];
            System.out.println("Введите элементы массива: ");
            for (int i = 0; i < array.length; i++) {
                array[i] = scanner.nextInt();
            }

            System.out.println("Сумма элементов массива " + sumDigitsArray(array));

            System.out.println("Среднее арифметическое массива: " + arrayAverage(array));

            swapInArray(array);
            System.out.println("Массив в зеркальном представлении: " + Arrays.toString(array));

            bubbleSort(array);
            System.out.println("Массив после пузырьковой сортировки : " + Arrays.toString(bubbleSort(array)));

            System.out.println("Массив после разворота max и min: " + Arrays.toString(reverseArray(array)));

            System.out.println("Массив преобразованный в число: " + transformationArray(array));
        }

        public static int arrayAverage(int[] array) {
            int average = 0;
            for (int i = 0; i < array.length; i++) {
                average += array[i];
            }
            return average / array.length;
        }

        public static int[] reverseArray(int[] array) {
            int max = array[0];
            int imax = 0;
            int min = array[0];
            int imin = 0;
            for (int i = 1; i < array.length; i++) {
                if (array[i] > max) {
                    max = array[i];
                    imax = i;
                }
                if (array[i] < min) {
                    min = array[i];
                    imin = i;
                }
            }
            array[imin] = max;
            array[imax] = min;
            return array;
        }
        public static int[] bubbleSort(int[] array) {
            int i;
            int temp;
            for (i = 0; i < array.length - 1; i++) {
                if (array[i] < array[i + 1]) {
                    temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                }
            }
            return array;
        }

        public static void swapInArray(int[] array) {
            int temp = 0;
            for (int i = 0; i < array.length / 2; i++) {
                temp = array[i];
                array[i] = array[array.length - i - 1];
                array[array.length - i - 1] = temp;
            }
        }

        public static int sumDigitsArray(int[] array) {
            int arraySum = 0;
            for (int i = 0; i < array.length; i++) {
                arraySum = arraySum + array[i];
            }
            return arraySum;
        }

        public static int transformationArray(int[] array) {
            int number = 0;
            int j = 0;
            for (int i = array.length - 1; i >= 0; --i, j++) {
                int pos = (int) Math.pow(10, i);
                number = number + array[j] * pos;
            }
            return number;
        }
    }


