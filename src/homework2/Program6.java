package homework2;

public class Program6 {
    public static void main(String[] args) {
        int[] array = { 1, 2, 3, 4, 5 };
        int number = 0;
        for (int i = array.length - 1, j = 0; i >= 0; --i, j++) {
            int pos = (int) Math.pow(10, i);
            number = number + array[j] * pos;
        }
        System.out.println(number);
    }

}
