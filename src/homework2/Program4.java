package homework2;

public class Program4 {
    public static void main(String[] args) {
        int array[] = { 100, 2, 78, 4, 45, 66, 54, -87 };
        int max = array[0];
        int imax = 0;
        int min = array[0];
        int imin = 0;
        for (int i = 1; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
                imax = i;
            }
            if (array[i] < min) {
                min = array[i];
                imin = i;
            }
        }
        array[imin] = max;
        array[imax] = min;
        for (int i = 0; i < array.length; i++) {
            System.out.println("i=" + i + " array[" + i + "]=" + array[i]);
        }
    }

}
