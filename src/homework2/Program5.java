package homework2;
import java.util.Arrays;

public class Program5 {
    public static void main(String[] args) {

        int[] array = { 10, 12, 23, -4, 5 };

        for (int i = array.length - 1; i > 0 ; i--) {
            for (int j = 0; j < i; j ++) {
                int pooledVal = 0;
                if (array[j] < array[j+1]) {
                    pooledVal = array[j];
                    array[j] = array [j+1];
                    array[j+1]=pooledVal;
                }
            }
        }
        System.out.println(Arrays.toString(array));
    }

}
