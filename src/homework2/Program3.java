package homework2;
import java.util.Scanner;

public class Program3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        int k = scanner.nextInt();
        System.out.println("Введите длину одномерного массива");
        System.out.println("Введите первый элемент массива");
        System.out.println("Водится массив");
        for (int i = 0; i < n; i++) {
            System.out.print(array[i] + k + "\t");
            k++;
        }
        System.out.println(" ");
        int arrayAverage = 0;
        for (int i = 0; i < n; i++) {
            arrayAverage += i;
        }
        arrayAverage /= n;
        System.out.println("Среднее арифметическое : " + arrayAverage);
    }

}
