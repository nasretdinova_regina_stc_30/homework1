package homework4;

public class Program {

    public static void main(String[] args) {
        int index = binarySearch(new int[]{79, 2, -7, 470, 57, 91}, 0, 6, 2);
        System.out.println(index);
        }

        public static int binarySearch(int[] array,int firstElement,int lastElement, int element){
            if (lastElement >= firstElement) {
                int middle = firstElement + (lastElement - firstElement) / 2;
                if (array[middle] == element)
                    return middle;
                if (array[middle] > element)
                    return binarySearch(array, firstElement, middle - 1, element);
                return binarySearch(array, middle + 1, lastElement, element);
            }
            return -1;
        }
    }


