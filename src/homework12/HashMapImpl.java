package homework12;

public class HashMapImpl<K, V> implements Map<K, V> {
    private static final int DEFAULT_SIZE = 16;

    private final MapEntry<K, V>[] entries = new MapEntry[DEFAULT_SIZE];

    private static class MapEntry<K, V> {
        K key;
        V value;
        MapEntry<K, V> next;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }


    @Override
    public void put(K key, V value) {
        MapEntry<K, V> newMapEntry = new MapEntry<>(key, value);
        int index = key.hashCode() & (entries.length - 1);
        if (entries[index] == null) {
            entries[index] = newMapEntry;
        } else {
            // если уже в этой ячейке (bucket) уже есть элемент
            // запоминаем его
            MapEntry<K, V> current = entries[index];
            // теперь дойдем до последнего элемента в этом списке
            while (current.next != null) {
                current = current.next;
            }
            // теперь мы на последнем элементе, просто добавим новый элемент в конец
            current.next = newMapEntry;
        }
    }

    @Override
    public V get(K key) {
        MapEntry<K, V> current;
        for (MapEntry<K, V> entry : entries) {
            current = entry;
            while (current != null) {
                if (current.key.equals(key)) {
                    return current.value;
                } else {
                    current = current.next;
                }
            }
            return null;
        }
        return null;
    }
}

