package homework7;

public class User {
    private String firstName;
    private String lastName;
    private int age;
    private boolean isWorker;

    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", isWorker=" + isWorker +
                '}';
    }

    public static class builder {
        private User newUser;

        public builder() {
            newUser = new User();
        }

        public builder firstName(String firstName) {
            newUser.firstName = firstName;
            return this;
        }

        public builder lastName(String lastName) {
            newUser.lastName = lastName;
            return this;
        }
        public builder age(int age) {
            newUser.age = age;
            return this;
        }
        public User build() {
            return newUser;
        }

        public builder isWorker(boolean isWorker) {
            newUser.isWorker = isWorker;
            return this;
        }
    }
}




