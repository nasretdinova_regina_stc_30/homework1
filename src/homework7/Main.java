package homework7;

public class Main {
    public static void main(String[] args) {
        User user = new User.builder().firstName("Marsel").lastName("Sidikov").age(26).isWorker(true).build();
        User user2 = new User.builder().firstName("Regina").lastName("Ivanova").age(20).isWorker(true).build();
        System.out.println(user.toString());
        System.out.println(user2.toString());
    }
}
