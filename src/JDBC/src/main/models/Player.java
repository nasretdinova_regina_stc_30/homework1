package JDBC.src.main.models;

public class Player {
    private Integer id;
    private String ip;
    private String name;
    private Integer max_score;
    private Integer win_count;
    private Integer loose_count;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMax_score() {
        return max_score;
    }

    public void setMax_score(Integer max_score) {
        this.max_score = max_score;
    }

    public Integer getWin_count() {
        return win_count;
    }

    public void setWin_count(Integer win_count) {
        this.win_count = win_count;
    }

    public Integer getLoose_count() {
        return loose_count;
    }

    public void setLoose_count(Integer loose_count) {
        this.loose_count = loose_count;
    }

    /**
     * Вспомогательный фасадный метод для обновления статиски с случае победы или проигрыша,
     * а так же вариативно в случае нового абсолютного максимума по очкам
     *
     * @param isWin победа или нет
     * @param score количество очков
     */
    public void addStat(boolean isWin, int score) {
        if (isWin) this.win_count++;
        else loose_count++;
        if (score > this.max_score) this.max_score = score;
        System.out.println("Статистика для " + this.name + " была успешно обновлена!");
    }

    public Player(String name, String ip) {
        this.ip = ip;
        this.name = name;
    }

    public Player() {
    }

    @Override
    public String toString() {
        return "Player{" +
                "id=" + id +
                ", ip='" + ip + '\'' +
                ", name='" + name + '\'' +
                ", max_score=" + max_score +
                ", win_count=" + win_count +
                ", loose_count=" + loose_count +
                '}';
    }
}