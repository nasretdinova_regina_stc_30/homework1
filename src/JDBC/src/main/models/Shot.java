package JDBC.src.main.models;



import java.util.Date;

public class Shot {
    private Integer id;
    private Date date;
    boolean isHit;
    private Game game;
    private Player attack;
    private Player defending;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public java.sql.Date getDate() {
        return (java.sql.Date) date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isHit() {
        return isHit;
    }

    public void setHit(boolean hit) {
        this.isHit = hit;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Player getAttack() {
        return attack;
    }

    public void setAttack(Player attack) {
        this.attack = attack;
    }

    public Player getDefending() {
        return defending;
    }

    public void setDefending(Player defending) {
        this.defending = defending;
    }

}