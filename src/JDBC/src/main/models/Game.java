package JDBC.src.main.models;


import java.sql.Time;
import java.time.LocalDateTime;

public class Game {
    private Integer id;
    private LocalDateTime effective_date;
    private Player a_player;
    private Player b_player;
    private Integer ap_shoots_count;
    private Integer bp_shoots_count;
    private Time duration;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDateTime getEffective_date() {
        return effective_date;
    }

    public void setEffective_date(LocalDateTime effective_date) {
        this.effective_date = effective_date;
    }

    public Player getA_player() {

        return a_player;
    }

    public void setA_player(Player a_player) {

        this.a_player = a_player;
    }

    public Player getB_player() {

        return b_player;
    }

    public void setB_player(Player b_player) {
        this.b_player = b_player;
    }

    public Integer getAp_shoots_count() {
        return ap_shoots_count;
    }

    public void setAp_shoots_count(Integer ap_shoots_count) {
        this.ap_shoots_count = ap_shoots_count;
    }

    public Integer getBp_shoots_count() {
        return bp_shoots_count;
    }

    public void setBp_shoots_count(Integer bp_shoots_count) {
        this.bp_shoots_count = bp_shoots_count;
    }

    public Time getDuration() {
        return duration;
    }

    public void setDuration(Time duration) {
        this.duration = duration;
    }

    public void addAPlayerShootingCounter() {
        this.ap_shoots_count++;
    }

    public void addBPlayerShootingCounter() {
        this.bp_shoots_count++;
    }

    public Game(Object now, Player firstPlayer, Player secondPlayer) {
    }

    public Game(LocalDateTime effective_date, Player a_player, Player b_player) {
        this.effective_date = effective_date;
        this.a_player = a_player;
        this.b_player = b_player;
    }
}