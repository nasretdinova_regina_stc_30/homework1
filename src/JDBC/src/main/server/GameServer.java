package JDBC.src.main.server;


import JDBC.src.main.service.GameService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class GameServer {
    private Socket firstPlayerSocket;
    private Socket secondPlayerSocket;

  private Integer game_id;
    private final GameService gameService;

 public GameServer(GameService gameService) {
        this.gameService = gameService;
    }

    private PlayerThread firstClient;
    private PlayerThread secondClient;

    private boolean isGameStarted = false;

    private final String TRY_SHOOT = ">>BANG>>";

    public void start(int port) {
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            System.out.println();
            System.out.println("*****************************************************");
            System.out.println("Сервер успешно запущен на хосте " + serverSocket.getInetAddress().getHostAddress()
                    + " на порту " + port);
            System.out.println("*****************************************************");
            while (!isGameStarted) {
                if (firstPlayerSocket == null) {
                    firstPlayerSocket = serverSocket.accept();
                    System.out.println("Игрок #1 подключен!");

                    firstClient = new PlayerThread(firstPlayerSocket);
                    firstClient.playerValue = "Client#1";
                    firstClient.ip = firstPlayerSocket.getInetAddress().getHostAddress();
                    firstClient.outgoing.println("Вы подключены, ваш идентификатор: " + firstClient.playerValue);
                    firstClient.start();
                } else {
                    secondPlayerSocket = serverSocket.accept();
                    System.out.println("Игрок #2 подключен!");

                    secondClient = new PlayerThread(secondPlayerSocket);
                    secondClient.playerValue = "Client#2";
                    secondClient.ip = secondPlayerSocket.getInetAddress().getHostAddress();
                    secondClient.outgoing.println("Вы подключены, ваш идентификатор: " + secondClient.playerValue);
                    secondClient.start();

                    isGameStarted = true;
                }
            }
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Класс вспомогательных процессов
     */
    private class PlayerThread extends Thread {
        private Socket player;
        private String namePlayer;
        private String ip;
        private String playerValue;

        private BufferedReader incoming;
        private PrintWriter outgoing;

        public PlayerThread(Socket player) {
            this.player = player;
            try {
                this.incoming = new BufferedReader(new InputStreamReader(player.getInputStream()));
                this.outgoing = new PrintWriter(player.getOutputStream(), true);
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }

        @Override
        public void run() {
            while (true) {
                try {
                    String rawMessage = incoming.readLine();
                    String incomingMessage = playerValue + " " + rawMessage;
                    if (rawMessage != null) {
                        System.out.println("Получено сообщение от " + player.getInetAddress().getHostAddress() +
                                ":" + player.getPort() + " --> " + incomingMessage);
                        String[] command = incomingMessage.split(" ");
                        // Логика начала игры: сигнал вида "Client% *START* %USERNAME%"
                        switch (command[1]) {
                            case "START":
                                if (command.length > 2) {
                                    this.namePlayer = command[2];
                                    if (firstClient.namePlayer != null && secondClient.namePlayer != null) {
                                        game_id = gameService.startGame(firstClient.namePlayer, firstClient.ip,
                                                secondClient.namePlayer, secondClient.ip);
                                        firstClient.outgoing.println("Игра началась, ваш оппонент " + secondClient.namePlayer);
                                        secondClient.outgoing.println("Игра началась, ваш оппонент " + firstClient.namePlayer);
                                    }
                                }
                                break;
                            /**
                             * Логика стрельбы и прочего формата "Client% *COMMAND* *ADDITIONAL_INFO*"
                             * Где возможны следующие варианты, например если стрелял Client#1:
                             * Client#1 SHOT_T - попытка выстрела, информируем второе сообщение с результатом,
                             * это сообщение-информирование для второго игрока
                             * Client#1 SHOT_R HIT - выстрел с результатом попадание
                             * Client#1 SHOT_R MISSED - выстрел с результатом промах
                             * Последние два сообщения уже фиксируем, время выстрела вычисляем так,
                             * время выстрела - время анимации которое необходимо для дистежние линии с врагом
                             */

                            case "SHOT_T":
                                if (command[0].equals("Client#1")) {
                                    secondClient.outgoing.println(TRY_SHOOT);
                                } else firstClient.outgoing.println(TRY_SHOOT);
                                break;

                            case "SHOT_R":
                                if (command[0].equals("Client#1")) {
                                    gameService.makeShot(game_id, firstClient.namePlayer, secondClient.namePlayer);
                                } else if (command[0].equals("Client#2")) {
                                    gameService.makeShot(game_id, secondClient.namePlayer, firstClient.namePlayer);
                                }
                                break;
                            default:
                                System.out.println("Неизвестная команда, инцедент зафиксирован!");
                                break;
                        }
                    }
                } catch (IOException e) {
                    throw new IllegalArgumentException(e);
                }
            }
        }
    }

}
