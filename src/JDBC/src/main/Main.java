package JDBC.src.main;


import JDBC.src.main.server.GameServer;
import JDBC.src.main.service.GameService;
import JDBC.src.main.service.GameServiceImpl;

public class Main {
    public static void main(String[] args) {

        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:postgresql://localhost:5432/postgres");
        config.setUsername("postgres");
        config.setDriverClassName("org.postgresql.Driver");
        config.setMaximumPoolSize(20);
        HikariDataSource dataSource = new HikariDataSource(config);
        GameService gameService = new GameServiceImpl(dataSource);

        GameServer gameServer = new GameServer(gameService);
        gameServer.start(8080);

  }
}
