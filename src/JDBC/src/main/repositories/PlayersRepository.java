package JDBC.src.main.repositories;

import JDBC.src.main.models.Player;

public interface PlayersRepository extends Repository<Player> {
    void update(Player player);

    Player findByName(String name);

    @Override
    Player save(Player player);
}