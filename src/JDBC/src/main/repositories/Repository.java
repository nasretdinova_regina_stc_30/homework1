package JDBC.src.main.repositories;

public interface Repository<T> {
    T save(T entity);
}