package JDBC.src.main.repositories;

import JDBC.src.main.models.Shot;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ShotsRepositoryImpl implements ShotsRepository {

    private DataSource dataSource;

    private static final String ADD_SHOT = "INSERT INTO shot(date, hit, game_id, attack, defending)" +
            "VALUES (?,?,?,?,?);";

    public ShotsRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Shot shot) {
        try (PreparedStatement statement = getStatementByQuery(ADD_SHOT)) {
            statement.setDate(1, shot.getDate());
            statement.setBoolean(2, shot.isHit());
            statement.setInt(3, shot.getGame().getId());
            statement.setInt(4, shot.getAttack().getId());
            statement.setInt(5, shot.getDefending().getId());
            if (statement.executeUpdate() == 0)
                throw new SQLException("Выстрел не был сохранен!");
            System.out.println("DEBUG: Выстрел от " + shot.getAttack().getName() + " в " +
                    shot.getDefending().getName() + " успешно сохранен!");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private PreparedStatement getStatementByQuery(String query) {
        try {
            Connection connection = dataSource.getConnection();
            return connection.prepareStatement(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
