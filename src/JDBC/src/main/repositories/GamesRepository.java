package JDBC.src.main.repositories;
import JDBC.src.main.models.Game;

public interface GamesRepository extends Repository<Game> {
    Game save(Game game);

    void update(Game game);
}