package JDBC.src.main.repositories;

import JDBC.src.main.models.Shot;

public interface ShotsRepository {
    void save(Shot shot);
}