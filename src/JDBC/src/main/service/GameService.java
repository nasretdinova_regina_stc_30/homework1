package JDBC.src.main.service;

public interface GameService {
    Integer startGame(String firstPlayerName, String firstIp, String secondPlayerName, String secondIp);

    void makeShot(Integer gameId, String attackPlayerName, String defendingPlayerName);

}