package JDBC.src.main.service;


import JDBC.src.main.models.Game;
import JDBC.src.main.models.Player;
import JDBC.src.main.repositories.*;

import javax.sql.DataSource;

public class GameServiceImpl implements GameService {
    private PlayersRepository playersRepository;
    private GamesRepository gamesRepository;
    private ShotsRepository shotsRepository;

    private Player firstPlayer;
    private Player secondPlayer;
    private Game game;
    private Object LocalDateTime;

    public GameServiceImpl(DataSource dataSource) {
        this.playersRepository = new PlayersRepositoryImpl((javax.activation.DataSource) dataSource);
        this.gamesRepository = new GamesRepositoryImpl((javax.activation.DataSource) dataSource);
        this.shotsRepository = new ShotsRepositoryImpl(dataSource);
    }

    @Override
    public Integer startGame(String firstPlayerName, String firstPlayerIp,
                             String secondPlayerName, String secondPlayerIp) {
        firstPlayer = getAndSavePlayerIfExist(firstPlayerName, firstPlayerIp);
        secondPlayer = getAndSavePlayerIfExist(secondPlayerName, secondPlayerIp);

        game = new Game(LocalDateTime, firstPlayer, secondPlayer);
        return gamesRepository.save(game).getId();
    }

    @Override
    public void makeShot(Integer gameId, String attackPlayerName, String defendingPlayerName) {

    }

    private Player getAndSavePlayerIfExist(String playerName, String playerIp) {
        Player candidate = playersRepository.findByName(playerName);
        if (candidate != null) candidate.setIp(playerIp);
        else candidate = new Player(playerName, playerIp);
        return playersRepository.save(candidate);
    }

}