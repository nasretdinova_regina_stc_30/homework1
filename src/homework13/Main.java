package homework13;


import homework13.DAO.*;
import homework13.model.Course;
import homework13.model.Lesson;
import homework13.model.Teacher;
import homework13.utils.IdGeneratorFileBasedImpl;
import java.util.Optional;
import java.util.Random;

public class Main {
    public static void main(String[] args) {

        Random random = new Random();


        IdGeneratorFileBasedImpl idGeneratorFileBasedImpl = new IdGeneratorFileBasedImpl("idLesson.txt");

        LessonDao lessonDao = new LessonDaoFileBasedImpl("lesson.txt", idGeneratorFileBasedImpl, "course.txt");


        IdGeneratorFileBasedImpl idGeneratorFileBasedImpl1 = new IdGeneratorFileBasedImpl("idTeacher.txt");

        TeacherDao teacherDao = new TeacherDaoFileBasedImpl("teacher.txt", idGeneratorFileBasedImpl1, "course.txt");


        IdGeneratorFileBasedImpl idGeneratorFileBasedImpl2 = new IdGeneratorFileBasedImpl("idCourse.txt");

        CourseDao courseDao = new CourseDaoFileBasedImpl("course.txt", "teacher.txt",
                "lesson.txt", idGeneratorFileBasedImpl2);

        Optional<Course> course = courseDao.find(58L);
        Course crs = course.get();
        System.out.println(crs.toString());
        System.out.println(crs.getLessonCourse().toString());

        Optional<Course> course1 = courseDao.find(36L);
        Course crs1 = course1.get();
        System.out.println(crs1.toString());
        System.out.println(crs1.getListTeacher().toString());

        Optional<Teacher> oTeacher = teacherDao.find(8L);
        Teacher teacher = oTeacher.get();
        System.out.println(teacher.toString());
        System.out.println(teacher.getCourseList().toString());

        Optional<Teacher> oTeacher1 = teacherDao.find(5L);
        Teacher teacher1 = oTeacher1.get();
        System.out.println(teacher1.toString());
        System.out.println(teacher1.getCourseList().toString());

        Optional<Lesson> oLesson = lessonDao.find(5L);
        Lesson lesson = oLesson.get();
        System.out.println(lesson.toString());
        System.out.println(lesson.getCourse().toString());

    }
}


