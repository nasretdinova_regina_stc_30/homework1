package homework13.utils;

public class IdGeneratorImMemoryImpl implements IdGenerator {
    private Long lastGeneratedId;

    public IdGeneratorImMemoryImpl() {
        this.lastGeneratedId = -1L;
    }

    @Override
    public Long nextId() {
        return ++this.lastGeneratedId;
    }
}
