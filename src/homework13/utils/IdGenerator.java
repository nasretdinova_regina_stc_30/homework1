package homework13.utils;

public interface IdGenerator {
    Long nextId();
}
