package homework13.model;

import java.time.LocalDateTime;

public class Lesson {
    private String nameLesson;
    private String Course;
    private LocalDateTime dateTimeOfTheLesson;

    public Lesson() {
    }

    public Lesson(String nameLesson, String course, LocalDateTime dateTimeOfTheLesson) {
        this.nameLesson = nameLesson;
        Course = course;
        this.dateTimeOfTheLesson = dateTimeOfTheLesson;
    }

    public String getNameLesson() {
        return nameLesson;
    }

    public void setNameLesson(String nameLesson) {
        this.nameLesson = nameLesson;
    }

    public String getCourse() {
        return Course;
    }

    public void setCourse(String course) {
        Course = course;
    }

    public LocalDateTime getDateTimeOfTheLesson() {
        return dateTimeOfTheLesson;
    }

    public void setDateTimeOfTheLesson(LocalDateTime dateTimeOfTheLesson) {
        this.dateTimeOfTheLesson = dateTimeOfTheLesson;
    }

    @Override
    public String toString() {
        return "Lesson{" +
                "nameLesson='" + nameLesson + '\'' +
                ", Course='" + Course + '\'' +
                ", dateTimeOfTheLesson=" + dateTimeOfTheLesson +
                '}';
    }
}
