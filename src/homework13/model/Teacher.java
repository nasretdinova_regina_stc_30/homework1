package homework13.model;

import java.util.List;
import java.util.Objects;

public class Teacher {
    String firstName;
    String lastName;
    int experience;
    String courseList;

    public Teacher() {
    }

    public Teacher(String firstName, String lastName, int experience, String courseList) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.experience = experience;
        this.courseList = courseList;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public String getCourseList() {
        return courseList;
    }

    public void setCourseList(String courseList) {
        this.courseList = courseList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Teacher teacher = (Teacher) o;
        return experience == teacher.experience &&
                Objects.equals(firstName, teacher.firstName) &&
                Objects.equals(lastName, teacher.lastName) &&
                Objects.equals(courseList, teacher.courseList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, experience, courseList);
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", experience=" + experience +
                ", courseList='" + courseList + '\'' +
                '}';
    }
}
