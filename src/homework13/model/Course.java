package homework13.model;

import java.time.LocalDateTime;

public class Course {
    private Long idCourse;
    private String nameCourse;
    private LocalDateTime dateFrom;
    private LocalDateTime dateTo;
    private Long listTeacher;
    private Long lessonCourse;

    public Course() {

    }

    public Course(Long idCourse, String nameCourse, LocalDateTime dateFrom,
                  LocalDateTime dateTo, Long listTeacher, Long lessonCourse) {
        this.idCourse = idCourse;
        this.nameCourse = nameCourse;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.listTeacher = listTeacher;
        this.lessonCourse = lessonCourse;
    }

    public Course(String nameCourse, LocalDateTime dateFrom, LocalDateTime dateTo, Long listTeacher, Long lessonCourse) {
        this.nameCourse = nameCourse;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.listTeacher = listTeacher;
        this.lessonCourse = lessonCourse;
    }

    public Long getIdCourse() {
        return idCourse;
    }

    public void setIdCourse(Long idCourse) {
        this.idCourse = idCourse;
    }

    public String getNameCourse() {
        return nameCourse;
    }

    public void setNameCourse(String nameCourse) {
        this.nameCourse = nameCourse;
    }

    public LocalDateTime getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(LocalDateTime dateFrom) {
        this.dateFrom = dateFrom;
    }

    public LocalDateTime getDateTo() {
        return dateTo;
    }

    public void setDateTo(LocalDateTime dateTo) {
        this.dateTo = dateTo;
    }


    public Long getListTeacher() {
        return listTeacher;
    }

    public void setListTeacher(Long listTeacher) {
        this.listTeacher = listTeacher;
    }

    public Long getLessonCourse() {
        return lessonCourse;
    }

    public void setLessonCourse(Long lessonCourse) {
        this.lessonCourse = lessonCourse;
    }

    @Override
    public String toString() {
        return "Course{" +
                "idCourse=" + idCourse +
                ", nameCourse='" + nameCourse + '\'' +
                ", dateFrom=" + dateFrom +
                ", dateTo=" + dateTo +
                ", listTeacher=" + listTeacher +
                ", lessonCourse=" + lessonCourse +
                '}';
    }

}
