package homework13.DAO;


import homework13.model.Course;
import homework13.utils.IdGenerator;
import javax.imageio.IIOException;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

// // data[0] -> id
//        // data[1] -> name
//        // data[2] -> price
//        // data[3] -> count
//        return new Product(Long.parseLong(data[0]),
//                data[1],
//                Double.parseDouble(data[2]),
//                Integer.parseInt(data[3]));
//    };

public class CourseDaoFileBasedImpl implements CourseDao {
    private String fileName;
    private IdGenerator idGenerator;
    private Course courseToStringMapper;
    private Long idCourse;

    private Mapper<Course, String> courseStringMapper = course ->
            course.getIdCourse() + " " +
                    course.getNameCourse() + " " +
                    course.getDateFrom() + " " +
                    course.getDateTo() + "\r\n";



    private Mapper<String, Course> stringToCourseMapper = string -> {
        String data[] = string.split(" ");
        return new Course();
    };


    public CourseDaoFileBasedImpl(String name, String s, String fileName, IdGenerator idGenerator) {
        this.fileName = fileName;
        this.idGenerator = idGenerator;
    }

    @Override
    public void save(Course course) {
        try {
            OutputStream outputStream = new FileOutputStream(fileName, true);
            outputStream.write(courseToStringMapper.toString().getBytes());
            outputStream.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IIOException e){
            throw new IllegalStateException(e);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Course entity) {

    }

    @Override
    public void update(Course entity) {

    }

    @Override
    public Optional<Course> find(long key) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            while (current != null) {
                String data[] = current.split(" ");
                Long existedId = Long.parseLong(data[0]);
                if (existedId.equals(idCourse)) {
                    Course course = stringToCourseMapper.map(current);
                    return Optional.of(course);
                }
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public List<Course> findAll() {
        try {
            List<Course> courses = new ArrayList<>();
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            while (current != null) {
                Course course = stringToCourseMapper.map(current);
                courses.add(course);
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return courses;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

}
