package homework13.DAO;

import java.util.List;
import java.util.Optional;

public interface CrudDao<T> {
    Optional<T> find(long key);
    List<T> findAll();
    void save(T entity);
    void delete(T entity);
    void update(T entity);
}
