package homework13.DAO;


import homework13.model.Teacher;
import homework13.utils.IdGeneratorFileBasedImpl;

import javax.imageio.IIOException;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TeacherDaoFileBasedImpl implements TeacherDao {

    private String fileName;
    private Teacher lessonToStringMapper;

        public TeacherDaoFileBasedImpl(String fileName, Teacher lessonToStringMapper) {
        this.fileName = fileName;
        this.lessonToStringMapper = lessonToStringMapper;
    }

    private Mapper<Teacher, String> teacherToStringMapper = teacher ->
            teacher.getFirstName() + " " +
                    teacher.getLastName() + " " +
                    teacher.getExperience() + "\r\n";

    private Mapper<String, Teacher> stringToTeacherMapper = string -> {
        String data[] = string.split(" ");
        return new Teacher();
    };

    public TeacherDaoFileBasedImpl(String s, IdGeneratorFileBasedImpl idGeneratorFileBasedImpl1, String s1) {
    }


    @Override
    public Optional<Teacher> find(long key) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            while (current != null) {
                String data[] = current.split(" ");
                Long existedId = Long.parseLong(data[0]);
                if (existedId.equals(existedId)) {
                    Teacher teacher = stringToTeacherMapper.map(current);
                    return Optional.of(teacher);
                }
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Teacher> findAll() {
        try {
            List<Teacher> teachers = new ArrayList<>();
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            while (current != null) {
                Teacher teacher = stringToTeacherMapper.map(current);
                teachers.add(teacher);
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return teachers;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

    @Override
    public void save(Teacher entity) {
        try {
            OutputStream outputStream = new FileOutputStream(fileName, true);
            outputStream.write(lessonToStringMapper.toString().getBytes());
            outputStream.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IIOException e){
            throw new IllegalStateException(e);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Teacher entity) {

    }

    @Override
    public void update(Teacher entity) {

    }
}

