package homework13.DAO;

public interface Mapper<X, Y> {
    Y map(X x);
}
