package homework13.DAO;

import homework13.model.Lesson;
import homework13.utils.IdGeneratorFileBasedImpl;

import javax.imageio.IIOException;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class LessonDaoFileBasedImpl implements LessonDao {

    private String fileName;
    private Lesson lessonToStringMapper;

    public LessonDaoFileBasedImpl(String fileName, Lesson lessonToStringMapper) {
        this.fileName = fileName;
        this.lessonToStringMapper = lessonToStringMapper;
    }

    private Mapper<Lesson, String> lessonStringMapper = lesson ->
            lesson.getNameLesson() + " " +
                    lesson.getCourse() + " " +
                    lesson.getDateTimeOfTheLesson() + "\r\n";


    private Mapper<String, Lesson> stringToLessonMapper = string -> {
        String data[] = string.split(" ");
        return new Lesson();
    };

    public LessonDaoFileBasedImpl(String s, IdGeneratorFileBasedImpl idGeneratorFileBasedImpl, String s1) {
    }


    @Override
    public Optional<Lesson> find(long key) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            while (current != null) {
                String data[] = current.split(" ");
                Long existedId = Long.parseLong(data[0]);
                if (existedId.equals(existedId)) {
                    Lesson lesson = stringToLessonMapper.map(current);
                    return Optional.of(lesson);
                }
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return Optional.empty();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Lesson> findAll() {
        try {
            List<Lesson> lessons = new ArrayList<>();
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
            String current = bufferedReader.readLine();
            while (current != null) {
                Lesson lesson = stringToLessonMapper.map(current);
                lessons.add(lesson);
                current = bufferedReader.readLine();
            }
            bufferedReader.close();
            return lessons;
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }


    @Override
    public void save(Lesson entity) {
        try {
            OutputStream outputStream = new FileOutputStream(fileName, true);
            outputStream.write(lessonToStringMapper.toString().getBytes());
            outputStream.close();
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IIOException e) {
            throw new IllegalStateException(e);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void delete(Lesson entity) {

    }

    @Override
    public void update(Lesson entity) {

    }
}
