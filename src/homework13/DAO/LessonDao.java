package homework13.DAO;

import homework13.model.Lesson;

public interface LessonDao extends CrudDao<Lesson> {

}
