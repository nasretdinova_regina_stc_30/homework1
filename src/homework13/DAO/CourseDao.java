package homework13.DAO;

import homework13.model.Course;

public interface CourseDao extends CrudDao<Course> {

}
