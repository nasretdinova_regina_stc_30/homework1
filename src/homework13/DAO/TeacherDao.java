package homework13.DAO;

import homework13.model.Teacher;

public interface TeacherDao extends CrudDao<Teacher> {

}
