package homework11;


public class ArrayList<D> implements List<D> {

    private static final int DEFAULT_SIZE = 10;
    private D data[];
    private int count;

    public ArrayList() {
        this.data = (D[]) new Object[DEFAULT_SIZE];
    }

    private class ArrayListIterator<D> implements Iterator<D> {

        private int current = 0;

        @Override
        public D next() {
            D value = (D) data[current];
            current++;
            return value;
        }

        @Override
        public boolean hasNext() {
            return current < count;
        }
    }

    @Override
    public D get(int index) {
        if (index < count) {
            return this.data[index];
        }
        System.err.println("Вышли за пределы массива");
        return null;
    }

    @Override
    public int indexOf(D element) {
        for (int i = 0; i < count; i++) {
            if (data[i] == element) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void removeByIndex(int index) {

        for (int i = index; i < count - 1; i++) {
            this.data[i] = this.data[i + 1];
        }
        this.count--;
    }

    @Override
    public void insert(D element, int index) {
        if (count == data.length) {
            resize();
        }

        for (int i = count - 1; i >= index; i--) {
            this.data[i + 1] = this.data[i];
        }
        data[index] = element;
        count++;

    }

    @Override
    public void reverse() { }


    @Override
    public boolean contains(D element) {
        return indexOf(element) != -1;
    }

    @Override
    public void add(D element) {
        if (count == data.length - 1) {
            resize();
        }
        data[count] = element;
        count++;
    }

    private void resize() {
        int oldLength = this.data.length;
        int newLength = oldLength + (oldLength >> 1);
        D newData[] = (D[]) new Object[newLength];

        System.arraycopy(this.data, 0, newData, 0, oldLength);

        this.data = newData;
    }


    @Override
    public int size() {
        return this.count;
    }

    @Override
    public void removeFirst(D element) {
        int indexOfRemovingElement = indexOf(element);

        for (int i = indexOfRemovingElement; i < count - 1; i++) {
            this.data[i] = this.data[i + 1];
        }

        this.count--;
    }

    @Override
    public Iterator<D> iterator() {
        return new ArrayListIterator<>();
    }

}


