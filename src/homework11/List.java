package homework11;

public interface List<B> extends Collection<B> {
    B get(int index);
    int indexOf(B element);
    void removeByIndex(int index);
    void insert(B element, int index);
    void reverse();

}
