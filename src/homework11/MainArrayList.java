package homework11;

public class MainArrayList {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < 9; i++) {
            list.add(i);
        }

          list.removeFirst(1);
        System.out.println(list.get(8));
        System.out.println(list.indexOf(11));
        System.out.println(list.size());
        System.out.println(list.contains(15));

        System.out.println("Список до удаления элемента");


        printList(list);

        list.removeByIndex(4);
        System.out.println("Список после удаления элемента");
        printList(list);

        System.out.println("Вставляем новый элемент");

        list.insert(100, 3);
        list.insert(100, 3);
        list.insert(100, 3);
        list.insert(100, 3);
        list.insert(100, 3);

        System.out.println("Список после вставки элементов");
        printList(list);


    }

    private static void printList(List list) {
        Iterator iterator;
        iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

}
