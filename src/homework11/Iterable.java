package homework11;

public interface Iterable<E> {
    Iterator<E> iterator();
}
