package homework11;


public class LinkedList<D> implements List<D> {

    private class LinkedListIterator implements Iterator<D> {
        Node<D> current =  first;
        Node<D> returned =  null;

        @Override
        public D next() {
            if (current != null && this.hasNext()) {
                returned = current;
                current = current.next;
                return returned.value;
            }
            return current.value;
        }

        @Override
        public boolean hasNext() {
            return current != null;
        }
    }



    private Node<D> first;
    private Node<D> last;

    private int count;
    private int element;

    private static class Node<F> {
        F value;
        Node<F> next;

        public Node(F value) {
            this.value = value;
        }
    }

    @Override
    public D get(int index) {
        int result = -1;
        if (index >= 0 && index < count && first != null) {
            int i = 0;
            Node current = this.first;

            while (i < index) {
                current = current.next;
                i++;
            }

            return (D) current.value;
        } else {
            System.err.println("Такого элемента нет");
        }

        return null;
    }


    @Override
    public int indexOf(D element) {
        int i = 0;
        Node current = this.first;

        while (current != null && current.value != element) {
            current = current.next;
            i++;
        }

        if (current == null) {
            return -1;
        } else {
            return i;
        }
    }


    @Override
    public void removeByIndex(int index) {
        Node current = this.first;
        Node current1 = current.next;
        if (index < count && index > 0) {
            for (int j = 0; j < index - 1; j++) {
                current = current.next;
                current1 = current.next;
            }
            current.next = current1.next;
            count--;
        } else if (index == 0) {
            this.first = current.next;
            count--;
        }
    }


    @Override
    public void insert(D element, int index) {
        Node current = this.first;
        Node current1 = current.next;
        Node newNode = new Node(element);
        if (index <= count && index > 0) {
            for (int j = 0; j < index - 1; j++) {
                current = current.next;
                current1 = current.next;
            }
            current.next = newNode;
            newNode.next = current1;
            count++;
        } else if (index == 0) {
            newNode.next = first;
            first = newNode;
            count++;
        } else {
            System.out.println("Данного индекса нет!");
        }
    }

    @Override
    public void reverse() {
        Node current = this.first;
        Node[] list = new Node[count];
        for (int i = count - 1; i >= 0; i--) {
            list[i] = current;
            current = current.next;
        }

        for (int i = 0; i < count - 1; i++) {
            insert((D) list[i].value,i);
            count--;
        }

    }

    @Override
    public void add(D element) {
        Node newNode = new Node(element);
        if (first == null) {
            first = newNode;
        } else {
            last.next = newNode;
        }
        last = newNode;
        count++;
    }


    @Override
    public boolean contains(D element) {
        return indexOf(element) >= 0;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public void removeFirst(D element) {
        Node current = this.first;
        Node current1 = current.next;


        while (current.next != null && current1.value != element) {
            current = current.next;
            current1 = current1.next;
        }
        if (first.value == element) {
            first = first.next;
            count--;
        } else if (current1 != null && current1.value == element) {
            current.next = current1.next;
            count--;
        }

    }

    @Override
    public Iterator<D> iterator() {
        return new LinkedListIterator();
    }
}
















